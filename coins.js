const getCoinsEarned = (spinCombination) => {
  const combination = spinCombination.join('');
  // 2 fruits which give points can be on start or end
  // order of times matched matters
  if (/(cherry){3}/.test(combination)) return 50;
  if (/(cherry){2}/.test(combination)) return 40;
  if (/(apple){3}/.test(combination)) return 20;
  if (/(apple){2}/.test(combination)) return 10;
  if (/(banana){3}/.test(combination)) return 15;
  if (/(banana){2}/.test(combination)) return 5;
  if (/(lemon){3}/.test(combination)) return 3;
  return 0;
};

module.exports = getCoinsEarned;
