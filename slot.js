const getCoinsEarned = require('./coins');
const getRandomNumber = require('./random');

const reel1 = ['cherry', 'lemon', 'apple', 'lemon', 'banana', 'banana', 'lemon', 'lemon'];
const reel2 = ['lemon', 'apple', 'lemon', 'lemon', 'cherry', 'apple', 'banana', 'lemon'];
const reel3 = ['lemon', 'apple', 'lemon', 'apple', 'cherry', 'lemon', 'banana', 'lemon'];

const spin = (coins = 20) => {
  const spinCombination = [
    reel1[getRandomNumber()],
    reel2[getRandomNumber()],
    reel3[getRandomNumber()],
  ];

  const coinsEarned = getCoinsEarned(spinCombination);

  coins += -1;
  coins += coinsEarned;

  return {
    spinCombination,
    coinsEarned,
    coins,
  }
};

module.exports = spin;
