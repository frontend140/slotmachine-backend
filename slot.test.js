const spin = require('./slot');

jest.mock('./coins', () => jest.fn(() => 3));
jest.mock('./random', () => jest.fn(() => 0));

describe('returns whole spin object', () => {
  it('spins starting with 0 coins', () => {
    expect(spin(0)).toStrictEqual({
      coins: 2,
      coinsEarned: 3,
      spinCombination: [
        'cherry', 'lemon', 'lemon',
      ],
    });
  });

  it('spins starting with 20 coins', () => {
    expect(spin(20)).toStrictEqual({
      coins: 22,
      coinsEarned: 3,
      spinCombination: [
        'cherry', 'lemon', 'lemon',
      ],
    });
  });
});
