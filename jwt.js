const jwt = require('jsonwebtoken');
const { getUser, setUser } = require('./fake_db');

const authenticateToken = (request, response, next) => {
  const authHeader = request.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (token == null) {
    return response.sendStatus(401);
  }

  jwt.verify(
    token,
    process.env.TOKEN_SECRET,
    (err, user) => {

      if (err) {
        console.log(err)
        return response.sendStatus(403);
      }

      const {password, ...data} = {...user};

      const dbUser = getUser({ username: data.username, password: password });

      if (!dbUser) {
        return response.sendStatus(403);
      }

      request.user = dbUser;

      next();
  });
}

const loginUser = ({ username, password }) => {
  const user = getUser({username, password});
  if (user) {
    return {
      username: user.username,
      email: user.email,
      coins: user.coins,
      token: generateAccessToken({
        username: user.username,
        password: user.password,
      }),
    };
  } else {
    return null;
  }
}

const registerUser = ({username, email, password}) => {
  setUser({username, email, password});
}

const generateAccessToken = ({username, password}) => {
  return jwt.sign(
    {username, password},
    process.env.TOKEN_SECRET,
    { expiresIn: '1d' },
  );
}

module.exports = {
  authenticateToken,
  loginUser,
  registerUser,
};
