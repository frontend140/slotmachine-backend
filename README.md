# slotmachine-backend

NodeJS API.

Requires
```
  node v16.10.0
  yarn 1.22
```

Create an .env file with:
```
TOKEN_SECRET={some token here}
```
This will be used to generate JWT tokens and later authenticate the user. JWT token is sent with every request as a part of request header.

To start the server, navigate to root folder of the project and run:
```
yarn start
```

Run tests with:
```
yarn test
```
