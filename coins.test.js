const getCoinsEarned = require('./coins');

describe('getCoinsEarned', () => {
  it('3 cherries give 50 coins', () => {
    expect(getCoinsEarned(['cherry', 'cherry', 'cherry'])).toBe(50);
  });

  it('2 cherries give 40 coins', () => {
    expect(getCoinsEarned(['cherry', 'cherry', 'lemon'])).toBe(40);
    expect(getCoinsEarned(['lemon', 'cherry', 'cherry'])).toBe(40);
    expect(getCoinsEarned(['cherry', 'lemon', 'cherry'])).toBe(0);
  });

  it('3 apples give 20 coins', () => {
    expect(getCoinsEarned(['apple', 'apple', 'apple'])).toBe(20);
  });

  it('2 apples give 10 coins', () => {
    expect(getCoinsEarned(['apple', 'apple', 'cherry'])).toBe(10);
    expect(getCoinsEarned(['cherry', 'apple', 'apple'])).toBe(10);
    expect(getCoinsEarned(['apple', 'cherry', 'apple'])).toBe(0);
  });

  it('3 bananas give 15 coins', () => {
    expect(getCoinsEarned(['banana', 'banana', 'banana'])).toBe(15);
  });

  it('2 bananas give 5 coins', () => {
    expect(getCoinsEarned(['apple', 'banana', 'banana'])).toBe(5);
    expect(getCoinsEarned(['banana', 'banana', 'apple'])).toBe(5);
    expect(getCoinsEarned(['banana', 'apple', 'banana'])).toBe(0);
  });

  it('3 lemons give 3 coins', () => {
    expect(getCoinsEarned(['lemon', 'lemon', 'lemon'])).toBe(3);
  });

  it('anything else gives 0 coins', () => {
    expect(getCoinsEarned(['cherry', 'apple', 'cherry'])).toBe(0);
  });
});
