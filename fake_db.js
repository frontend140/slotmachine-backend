const users = [
  {username: 'demo', email: 'demo@gmail.com', password: '123', coins: 20},
];

const setUser = user => {
  users.push(user);
}

const updateUserCoins = (user, coins) => {
  const dbUser = getUser(user);
  if (dbUser) {
    dbUser.coins = coins;
  }
}

const getUser = user => {
  return users.find(u => {
    return (
      u.username === user.username &&
      u.password === user.password
    );
  });
}

module.exports = {
  setUser,
  getUser,
  updateUserCoins,
};
