const dotenv = require('dotenv');
const express = require('express');
const cors = require('cors');
const spin = require('./slot');
const bodyParser = require('body-parser');
const { authenticateToken, loginUser, registerUser } = require('./jwt');
const { updateUserCoins } = require('./fake_db');
const app = express();
const port = process.env.PORT || 3000;

dotenv.config();

app.use(cors({ origin: true }));
app.use(bodyParser.json());

app.get('/spin', authenticateToken, (request, response) => {
  const spinResult = spin(request.user.coins);
  updateUserCoins(request.user, spinResult.coins);
  response.send(spinResult);
});

app.post('/login', (request, response) => {
  const user = loginUser({
    username: request.body.username,
    password: request.body.password,
  });

  if (user) {
    response.send({
      username: user.username,
      email: user.email,
      token: user.token,
    });
  } else {
    response.sendStatus(401);
  }
});

app.post('/register', (request, response) => {
  const user = registerUser({
    username: request.body.username,
    email: request.body.email,
    password: request.body.password,
  });
  response.sendStatus(200);
});

app.listen(port, () => console.log(`Listening on port ${port}`));
